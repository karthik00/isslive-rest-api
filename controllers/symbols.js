var metadata = require('../config').metadata;
var _ = require('underscore');

module.exports = function() {
	var symbols = [];
	_(metadata).each(function(v, k) {
		_(v).each(function(v1, k1) {
			_(v1).each(function(x) {
				symbols.push(x.name);
			});
		});
	});

	symbols = _(symbols).uniq().sort();

	this.actions = [{method: 'get', path: 'list', handler: function(req, res) { res.json(symbols); }}];
};