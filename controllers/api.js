var metadata = require('../config').metadata;
var Sensor = require('../models/sensor');
var _ = require('underscore');

module.exports = function(db) {
	var sensor = new Sensor(db);

	var list = function(req, res) {
		if(req.param('format') == 'xml') {
			// Yet to implement
		} else {
			res.json({code: 1});
		}
	};

	var get = function(req, res) {
		if(req.param('format') == 'xml') {
			// Yet to implement
		} else {
			res.json({code: 1});
		}
	};

	var getListHandler = function(meta) {
		return function(req, res) {
			var sensors = _(meta).map(function(item) { return item.name; });
			sensor.list(sensors, function(data) {
				_(data).each(function(x) {
					var i = _(meta).find(function(t) { return t.name == x.Name; });
					x.Alias = i.alias;
				});
				res.json(data);
			});
		};
	};

	var getItemHandler = function(item) {
		return function(req, res) {
			sensor.get(item.name, function(data) {
				data.Alias = item.alias;
				res.json(data);
			});
		};
	};

	var actions = this.actions = [];

	_(metadata).each(function(val, key) {
		var t = val;
		_(t).each(function(val1, key1) {
			var s = val1;
			_(s).each(function(item) {
				actions.push({method: 'get', path: key + '/' + key1 + '/' + item.name, handler: getItemHandler(item)});
				actions.push({method: 'get', path: key + '/' + key1 + '/' + item.alias, handler: getItemHandler(item)});
			});
			actions.push({method: 'get', path: key + '/' + key1, handler: getListHandler(s)});
		});
	});

}