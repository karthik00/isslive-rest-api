// var Schema = require('mongoose').Schema;
// var ObjectId = Schema.ObjectId;
// 
// var Status = new Schema({
// 	Class: Number,
// 	Indicator: String,
// 	Color: String
// });
// 
// var SensorValue = new Schema({
// 	Value: Number,
// 	TimeStamp: Number
// });
// 
// var Sensor = new Schema({
// 	id: ObjectId,
// 	Name: String,
// 	Status: Status,
// 	Data: [SensorValue],
// 	CalibratedData: String,
// 	last_updated: Number
// });

module.exports = function(db) {	

	this.list = function(sensors, callback) {
		var pos = 0;
		var allData = [];
		var doIt = function(data) {
			if(data) allData.push(data);
			if(pos < sensors.length) {
				console.log(sensors[pos]);
				get(sensors[pos], doIt);
			} else {
				callback(allData);
			}
			pos++;
		};
		doIt();	
	};

	var get = this.get = function(sensorId, callback) {
		db.collection(sensorId, function(err, collection) {
			if(err) throw err;
			collection.findOne({}, {sort: [['last_updated', -1]], limit: 1}, function(err, doc) {
				if(err) throw err;
				if(doc) {
					callback({Name: sensorId, Value: doc.Data[0].Value, CalibratedData: doc.CalibratedData, LastUpdated: doc.last_updated});
				} else {
					callback({Error: "Symbol not found."});
				}
			});
		});
	};
};