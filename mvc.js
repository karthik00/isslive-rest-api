var express = require('express');
var config = require('./config');
var Db = require('mongodb').Db;
var Connection = require('mongodb').Connection;
var Server = require('mongodb').Server;
var fs = require('fs');

exports.Application = function(port) {
	var app = express.createServer();

	app.configure(function() {
		app.use(express.bodyParser());
		app.use(express.methodOverride());
		app.use(express.cookieParser());
		app.use(express.session({secret: config.sessionsecret}));
		app.use(app.router);
//		app.use(express.static(__dirname + '/public'));
	});

	app.configure('development', function() {
		app.use(express.logger(':method :url :status'));
		app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
	});

	app.configure('production', function() {
		app.use(express.errorHandler()); 
	});

	var db = new Db('ISSLive', new Server(config.mongoHost, Connection.DEFAULT_PORT, {}), { native_parser: false });
	db.open(function(err, db) {
		if(err) throw err;
		fs.readdir(__dirname + '/controllers', function(err, files) {
			if(err) throw err;
			files.forEach(function(file) {
				var name = file.replace('.js', '');
				var Controller = require('./controllers/' + name);
				var controller = new Controller(db);
				controller.actions.forEach(function(action) {
					app[action.method]('/' + name + '/' + action.path, action.handler);
				});
			});
		});
	});

	this.start = function() {
		app.listen(port);
		console.log("Express started on port %d in %s mode", app.address().port, app.settings.env);
	};
}
